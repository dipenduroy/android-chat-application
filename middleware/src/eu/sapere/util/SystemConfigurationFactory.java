package eu.sapere.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * The System Configuration Factory, uses reflection.
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class SystemConfigurationFactory {

	/**
	 * Instantiates the System Configuration.
	 * 
	 * @return An object implementing the ISystemConfiguration interface.
	 */
	public static ISystemConfiguration getSystemConfiguration() {

		Class<?> c = null;
		try {
			c = Class.forName("eu.sapere.util.SystemConfiguration");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Method m = null;
		try {
			Class<?>[] cp = null;
			m = c.getDeclaredMethod("getInstance", cp);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		m.setAccessible(true); // if security settings allow this
		Object o = null;
		try {
			Object o1 = null;
			Object o2[] = null;
			o = m.invoke(o1, o2);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (ISystemConfiguration) o;
	}

}