package eu.sapere.middleware.node.networking.transmission.protocols.tcpip;

import java.net.Socket;
import java.util.HashMap;

public class ConnectionManager 
{
	private static ConnectionManager singleton=null;
	private HashMap<String, Socket> sockets;
	
	private ConnectionManager()
	{
		sockets = new HashMap<String, Socket>();
	}
	
	public static ConnectionManager getInstance()
	{
		if (singleton == null)
		{
			singleton = new ConnectionManager();	
			return singleton;
		}
		else
			return singleton;
	}

	public synchronized void addSocket(Socket socket, String ip, int port)
	{
		sockets.put(ip+ ":" + port, socket );
	}
	
	public synchronized Socket getSocketConnection(String ip, int port)
	{
		if (sockets.containsKey(ip+ ":"))
			return sockets.get(ip+ ":");
		else
		{
			try
			{
				Socket socket = new Socket(ip, port);
				sockets.put(ip+ ":", socket);
				return socket;
			}
			catch(Exception ex)
			{
				return null;
			}
		}
	}

	
}
