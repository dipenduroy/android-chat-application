package eu.sapere.middleware.node.networking.transmission.protocols.tcpip;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import eu.sapere.middleware.lsa.interfaces.ILsa;

public class ThreadSender extends Thread
{
	// queue of lsa waiting for being sent
	private Queue<ILsa> queue;
	// ip and port of the destination node
	//
	private String ipDest;
	private int port;
	private final int pollTime =60;
	private PoolThread pool;
	private Boolean on;
	public ThreadSender(PoolThread pool, String ipDest, int port)
	{
		this.ipDest = ipDest;
		this.port = port;
		this.pool = pool;
		queue = new LinkedList<ILsa>();
		on=true;
	}
	
	public void run()
	{
		// get the next element
		while(on)
		{
			try
			{
				ILsa nextLsa;
				synchronized(this)
				{
					nextLsa = queue.poll();
					
					if (nextLsa == null) 
					{
						this.wait();
						continue;
					}
				}
				// send this lsa
				sendLsa(nextLsa);
			}
			catch(InterruptedException ex)
			{
				//on = false;
			}
		}
		// send a message to the pool
		pool.onThreadExit(this);
	}
	
	public synchronized void pushLsa(ILsa lsa)
	{
		queue.add(lsa);
		this.notify();
	}
	
	private void sendLsa(ILsa lsa)
	{
		try
		{
			Socket socket = new Socket();
			socket.setSoTimeout(2000);
			socket.connect( new InetSocketAddress(ipDest, port));
			ObjectOutputStream oos = new ObjectOutputStream(
					socket.getOutputStream());
				oos.writeObject(lsa);	
			socket.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			pool.onThreadError(this);
		}
	}
	
	public synchronized void forceThreadStop()
	{
		on = false;
		this.notify();
	}
	
	
	public String getIpNode()
	{
		return this.ipDest;
	}
	
	public int getPortNode()
	{
		return this.port;
	}
}
