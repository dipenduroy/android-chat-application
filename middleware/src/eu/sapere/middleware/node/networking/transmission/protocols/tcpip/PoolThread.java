package eu.sapere.middleware.node.networking.transmission.protocols.tcpip;

import java.util.ArrayList;
import java.util.HashMap;

import eu.sapere.middleware.lsa.interfaces.ILsa;

public class PoolThread 
{
	private HashMap<String, ThreadSender> senders;
	private static PoolThread singleton=null;
	public static int nInstances = 0;
	public static int nThread = 0;
	
	public static PoolThread getInstance()
	{
		if (singleton == null)
			singleton = new PoolThread();
		nInstances++;
		return singleton;
	}
	
	private PoolThread()
	{
		senders = new HashMap<String,ThreadSender>();
	}
	
	public synchronized void addNewThread(String ip, int port)
	{
		if (senders.containsKey(ip)) return;
		
		ThreadSender sender = new ThreadSender(this,ip,port);
		// create a new thread sender
		senders.put(ip, sender);
		sender.start();
		nThread++;
	}
	
	public synchronized void pushLsa(ILsa lsa, String ipDest)
	{
		// push an lsa into the queue of the associated thread sender
		senders.get(ipDest).pushLsa(lsa);
	}
	
	public void onThreadError(ThreadSender s)
	{
		
	}
	
	public synchronized void onThreadExit(ThreadSender s)
	{
		this.senders.remove(s);
	}
	
	public synchronized void removeSenderThread(String ipDest)
	{
		ThreadSender sender = this.senders.get(ipDest);
		if (sender == null) return;
		// stop the thread
		sender.forceThreadStop();
	}
}
