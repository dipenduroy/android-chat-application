package eu.sapere.middleware.agent.remoteconnection;

import java.io.*;
import java.net.*;

import eu.sapere.middleware.lsa.Lsa;

/**
 * The thread to manage an incoming connection to the Server
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class ThreadServer extends Thread {

	/** The client sockect */
	Socket client_socket;

	/** The output stream */
	ObjectOutputStream oos;

	/** The input stream */
	ObjectInputStream ois;

	/** Used to close the connection */
	boolean active;

	/** The Remote Sapere Agent that will manage the icoming LSA */
	private RemoteSapereAgent agent = null;

	/**
	 * Instantiates the thread
	 * 
	 * @param client_socket
	 *            The client sockect
	 * @param OpMng
	 *            The local Operation Manager
	 * @param notifier
	 *            The local Notifier
	 */
	public ThreadServer(Socket client_socket) {

		// System.out.println("Connection accepted from:"+client_socket.getInetAddress());
		this.client_socket = client_socket;
		agent = new RemoteSapereAgent("remoteAgent_" + this.getId(), this);

	}

	public void run() {

		try {
			Object input;
			oos = new ObjectOutputStream(client_socket.getOutputStream());
			ois = new ObjectInputStream(client_socket.getInputStream());

			// System.out.println("Thread Server Waiting for messages...");

			try {
				while (((input = ois.readObject()) != null)
						&& (!input.equals("clientLeaving"))) {

					// System.out.println("Lsa received by ThreadServer");
					// System.out.println(input);
					Lsa lsa = (Lsa) input;
					agent.setLsa(lsa);

					if (agent.lsaId == null)
						agent.injectOperation();
					else
						agent.updateOperation();

				}

				if (input.equals("clientLeaving"))
					if (agent.lsaId != null) {
						agent.removeLsa();
					}
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
	}

	/**
	 * Stops the thread
	 */
	public void stopThread() {

		if (oos != null) {
			try {
				oos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (ois != null) {
			try {
				ois.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (client_socket != null) {
			try {

				client_socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * Forwards local Events to the Remote agent that manages remotely the local
	 * LSA
	 * 
	 * @param event
	 *            The event to be forwarded
	 */
	public void forwardEvent(Object event) {

		// System.out.println("forward event to remote proxy");

		// This is an event to be forwarded
		try {
			oos.writeObject(event);
			// oos.reset();
			// oos.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}

	}

}