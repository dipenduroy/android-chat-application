/*
 * A thread that recraetes a new SecuredChannel each 10 seconds
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 */
package sapere;

public class ChannelManagerThread extends Thread
{
	private String ip;
	private String nodeName;
	private String encodedKey;
	public ChannelManagerThread(String ip, String nodeName, String encodedKey)
	{
		this.ip = ip;
		this.nodeName = nodeName;
		this.encodedKey = encodedKey;
	}
		
	public void run()
	{
		int nMessages = 0;
		while(true)
		{
			try 
			{
				// inject a new gradient spreading the public key
				AgentGradient agent = new AgentGradient(
			     		  ip,
			     		  nodeName,nMessages, nodeName + "id" + nMessages,
			     		  "HelloMex", encodedKey);
			  agent.setInitialLSA();
			  
			  Thread.sleep(10000);				
			}
			catch(Exception ex){}
			nMessages++;
		}
	}
}