package eu.sapere.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import eu.sapere.demoChat.R;
import eu.sapere.android.SapereConsoleActivity;

/**
 * Provides the System Configuration singleton for Android devices. The
 * configuration file is locates in res/raw/settings
 * 
 * @author Gabriella Castelli (UNIMORE)
 * 
 */
public class SystemConfiguration extends AbstractSystemConfiguration {

	// private constructor for the singleton object
	private SystemConfiguration() {

		properties = new Properties();

		try {
			InputStream fis = (InputStream) SapereConsoleActivity.getContext()
					.getResources().openRawResource(R.raw.settings);
			properties.load(fis);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Retrieves current system configuration.
	 * 
	 * @return The configuration.
	 */
	public final static SystemConfiguration getInstance() {
		if (instance == null)
			instance = new SystemConfiguration();
		return (SystemConfiguration) instance;
	}

}