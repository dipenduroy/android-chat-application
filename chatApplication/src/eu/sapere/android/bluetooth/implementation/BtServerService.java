package eu.sapere.android.bluetooth.implementation;

import java.io.IOException;
import java.util.UUID;

import eu.sapere.android.bluetooth.implementation.BtManager.BTResponseReceiver;
import eu.sapere.middleware.node.NodeManager;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;
/**
 * The BtServerService is an IntentService managing Bluetooth ServerSocket
 * connectivity. Given device adapter limitations, it opens and manages only one
 * BT socket at time.
 * <p>
 * Once the connection is established, the {@link ServerConnEstab} class makes
 * Sapere devices to exchange middleware parameters.
 * <p>
 * Once the connection terminates the BtServerService goes in Destroy state; the
 * notifyEnd() method notifies the {@link BtManager} of such event.
 * 
 * @author Alberto Rosi
 */

public class BtServerService extends IntentService {

	private static final String TAG = "App_BtServerService";
	private static Object monitor = new Object();

	static Intent broadcastIntent = new Intent();
	static BtServerService myinstance;
	static BluetoothSocket socket;
	static BluetoothServerSocket mmServerSocket;
	static BluetoothAdapter mBluetoothAdapter;
	static String PARAM_OUT_MSG = "out_message";

	AddressDB ab;

	final UUID SapereUUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");

	String space_name;
	String space_type;

	/**
	 * Basic empty IntentService constructor calling onCreate() routine.
	 */
	public BtServerService() {
		super("BtServerService");
	}

	/**
	 * Retrieves parameters used in {@link ServerConnEstab} class.
	 */
	public void onCreate() {
		super.onCreate();

		ab = ((AddressDB) getApplicationContext());
		myinstance = this;

		// Check for SpaceName() not null, important in testing phase
		if (NodeManager.instance() != null)
			space_name = NodeManager.getSpaceName();
		else
			space_name = "space_name";
		space_type = "mobile_node";
	}

	/**
	 * On startService() the BtServerService opens a Bluetooth Server Socket and
	 * waits for incoming connections. Once it happens, a new Thread based on
	 * the {@link ServerConnEstab} class starts. The intent terminates once the
	 * ServerConnEstab thread terminates.
	 */

	protected void onHandleIntent(Intent intent) {

		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		try {
			mmServerSocket = mBluetoothAdapter
					.listenUsingRfcommWithServiceRecord("SapereService",
							SapereUUID);

			Log.d(TAG,
					"--> BtServerService deployed...  waiting for a new connection\n");

			socket = mmServerSocket.accept();

			mmServerSocket.close();

			if (socket != null) {
				// Do work to manage the connection (in a separate thread)

				synchronized (monitor) {
					Log.d(TAG, "Connection accepted");
					ServerConnEstab thread = new ServerConnEstab(socket,
							monitor, ab, space_name, space_type);
					thread.start();
					try {
						monitor.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						// Log.e(TAG, "Level1 " + e.getMessage());
					}
				}
			}

		} catch (IOException e) {

			Log.w(TAG, "BTSSocket closed!" + e.getMessage());

			// monitor.notifyAll();

			if (mmServerSocket != null) {

				try {
					mmServerSocket.close();
				} catch (IOException p) {
					// TODO Auto-generated catch block
					Log.e(TAG, e.getMessage());
				}
			}
			if (socket != null)
				try {
					socket.close();
				} catch (IOException f) {
					// TODO Auto-generated catch block
					Log.e(TAG, f.getMessage());
				}
		}

		notifyEnd();
	}

	private static BtServerService getInstance() {
		return myinstance;
	}

	/**
	 * Closes each active BT connections.
	 */
	public void onDestroy() {

		Log.d(TAG, "Server stop");

		if (mmServerSocket != null)
			try {
				mmServerSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e(TAG, e.getMessage());
			}
		if (socket != null)
			try {
				socket.close();
			} catch (IOException f) {
				// TODO Auto-generated catch block
				Log.e(TAG, f.getMessage());
			}

	}

	/**
	 * Notify {@link BtManager} that the current instance of BtServerService has
	 * terminated.
	 */
	public static void notifyEnd() {

		Log.d(TAG, "Server notify end");
		broadcastIntent.setAction(BTResponseReceiver.FINISH_RESP_SERVER);
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		getInstance().sendBroadcast(broadcastIntent);

	}

	/**
	 * Send a String to classes implementing the ResponseReceiver broadcaster.
	 * Typically to test GUI activity.
	 * 
	 * @param string
	 *            the string to be displayed.
	 */
	/*
	 * public static void sendMsg(String string) {
	 * 
	 * Log.d(TAG, "Server send back msg");
	 * 
	 * broadcastIntent.setAction(ResponseReceiver.ACTION_RESP);
	 * broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
	 * broadcastIntent.putExtra(PARAM_OUT_MSG, string);
	 * getInstance().sendBroadcast(broadcastIntent);
	 * 
	 * }
	 */
}
