/*
 * The Android Chat Application.
 * 
 * It establishes a SecuredChannel between two tablets.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 */

package eu.sapere.android;
import java.security.PublicKey;
import java.util.HashMap;


import sapere.AgentChemo;
import sapere.ChannelManagerThread;
import sapere.Global;
import securitymanager.EncryptedMessage;
import securitymanager.KeyManager;
import tools.PrinterDebug;
import eu.sapere.demoChat.R;
import eu.sapere.android.SapereMiddlewareIntent;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.graphics.*;
import android.view.View;

public class SapereConsoleActivity extends Activity {

	public static boolean ON_CLICK = false;
	public static TextView myView;
	public static HashMap<String,String> previous;
	private ConsoleLsaReceiver lsaReceiver;
	private ConsoleDebugReceiver debugReceiver;
	private ConsoleColorReceiver colorReceiver;
	private ConsoleGradientReceiver gradientReceiver;
	private ConsoleDebugMessagesReceiver debMexReceiver;
	private ConsoleLogReceiver logReceiver;
	private ConsoleIdReceiver idReceiver;
	private ConsoleDisableButton buttonReceiver;
	private static Context context = null;
	private tools.AndroidConfigReader reader;
	public static int nMessages = 0;
	// decrypt incoming messages
	public static KeyManager keyDecrypt;
	// encrypt outgoing messages
	public static PublicKey keyEncrypt=null;
	private Intent msgIntent = null;
	private String nodeName;
	public static String gradientId;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		PrinterDebug.c = context;
		previous = new HashMap<String,String>();
		try
		{
			tools.AndroidConfigReader r = new tools.AndroidConfigReader();
			// if (r.getNodeName().equals(Global.NODE_RECEIVER1) || r.getNodeName().equals(Global.NODE_RECEIVER2))
			keyDecrypt = new KeyManager();
			nodeName = r.getNodeName();
		}
		catch(Exception ex)
		{
			PrinterDebug.printConsole(ex.getMessage());
		}
		// Prevent screen from going to sleep
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_activity_console);

		myView = (TextView) findViewById(R.id.textLsa);
		myView.setMovementMethod(new ScrollingMovementMethod());
		TextView myView2 = (TextView) findViewById(R.id.textChatLog);
		myView2.setMovementMethod(new ScrollingMovementMethod());

		//myView.setText("WaitinA for LSAs...\n\n");

		IntentFilter filter2 = new IntentFilter(ConsoleLsaReceiver.ACTION_RESP);
		filter2.addCategory(Intent.CATEGORY_DEFAULT);
		lsaReceiver = new ConsoleLsaReceiver();
		registerReceiver(lsaReceiver, filter2);
		
		IntentFilter filter3 = new IntentFilter(ConsoleDebugReceiver.ACTION_RESP);
		filter3.addCategory(Intent.CATEGORY_DEFAULT);
		debugReceiver = new ConsoleDebugReceiver();
		registerReceiver(debugReceiver, filter3);
		
		IntentFilter filter4 = new IntentFilter(ConsoleColorReceiver.ACTION_RESP);
		filter4.addCategory(Intent.CATEGORY_DEFAULT);
		colorReceiver = new ConsoleColorReceiver();
		registerReceiver(colorReceiver, filter4);
		
		IntentFilter filter5 = new IntentFilter(ConsoleIdReceiver.ACTION_RESP);
		filter5.addCategory(Intent.CATEGORY_DEFAULT);
		idReceiver = new ConsoleIdReceiver();
		registerReceiver(idReceiver, filter5);
		
		IntentFilter filter6 = new IntentFilter(ConsoleGradientReceiver.ACTION_RESP);
		filter6.addCategory(Intent.CATEGORY_DEFAULT);
		gradientReceiver = new ConsoleGradientReceiver();
		registerReceiver(gradientReceiver, filter6);
		
		IntentFilter filter7 = new IntentFilter(ConsoleLogReceiver.ACTION_RESP);
		filter7.addCategory(Intent.CATEGORY_DEFAULT);
		logReceiver = new ConsoleLogReceiver();
		registerReceiver(logReceiver, filter7);
		
		IntentFilter filter8 = new IntentFilter(ConsoleDisableButton.ACTION_RESP);
		filter8.addCategory(Intent.CATEGORY_DEFAULT);
		buttonReceiver = new ConsoleDisableButton();
		registerReceiver(buttonReceiver, filter8);
		
		
		IntentFilter filter9 = new IntentFilter(ConsoleDebugMessagesReceiver.ACTION_RESP);
		filter9.addCategory(Intent.CATEGORY_DEFAULT);
		debMexReceiver = new ConsoleDebugMessagesReceiver();
		registerReceiver(debMexReceiver, filter9);
		

		// create the key manager and generate the public/private key pair
		//
		Log.d("eu.sapere", "Launching MiddlewareIntent!");
		msgIntent = new Intent(this, SapereMiddlewareIntent.class);
		startService(msgIntent);
		
		reader = new tools.AndroidConfigReader();
 
		// draw the start button if we are the sender n 1 
		//if (reader.getNodeName().equals(Global.NODE_SENDER1))
		if (true)		
		{
			Button buttonSend = (Button) findViewById(R.id.buttonSend);
			OnClickListener clickListener = new OnClickListener() 
			{
			    public void onClick(View v) 
			    {
			     Button buttonSend = (Button) v;
			    	 if (buttonSend.getText().equals("Send") == false)
			    	 {
			    		 new ChannelManagerThread(reader.getNodeIp(), nodeName, keyDecrypt.getEncodedPKey()).start();
					  
				    	 buttonSend.setText("Send");
				    	 return;
			    	 } 
			    	 if (keyEncrypt == null) return;
			    	 String encrypted = getEncMex();
			    	 String finalDestination,back;
			    	 if(nodeName.equals(Global.NODE_SENDER1))
			    	 {
			    		 finalDestination = Global.NODE_SENDER2;
			    	 }
			    	 else
			    	 {
			    		 finalDestination = Global.NODE_SENDER1;
			    	 }		 
			    	 
			    	 // clean the edit box
			    	 EditText textChat = (EditText) findViewById(R.id.editChatText);
				 textChat.setText("");

			    	 // send a message
	            	 AgentChemo chemo = 	new AgentChemo(encrypted, previous.get(gradientId),nodeName,finalDestination,
	            			 nMessages, nodeName + "Injector", nodeName,SapereConsoleActivity.gradientId,
	            			 "true");
	            	 nMessages++;
	            	 chemo.setInitialLSA();
			    }
			};
			buttonSend.setOnClickListener(clickListener);
		}
		else
		{
			// disable the sending button
			//
			Button buttonSend = (Button) findViewById(R.id.buttonSend);
			buttonSend.setVisibility(View.INVISIBLE);
			
			EditText textChat = (EditText) findViewById(R.id.editChatText);
			textChat.setVisibility(View.INVISIBLE);
			
			TextView log = (TextView) findViewById(R.id.textChatLog);
			log.setMaxLines(21);
		}
	}
	public String getEncMex()
	{
		// get the text end encrypt it
		EditText text = (EditText)findViewById(R.id.editChatText);
		String color;
		if (nodeName.equals(Global.NODE_SENDER1))
			color=Global.COLOR_CHANNEL_1;
		else
			color=Global.COLOR_CHANNEL_2;
		
		PrinterDebug.printLog(nodeName + ": " + text.getText().toString(),color);
   	    //PrinterDebug.printConsole("Invi mex " + text.getText().toString());
		return generateEncMex(text.getText().toString());
	}

	public static Context getContext() {
		return context;
	}

	private String generateEncMex(String toEncrypt)
	{
		// generate an encrypted message
		String encMex = generateEncMex(nodeName, toEncrypt);
		return encMex;
	}
	
	private String generateEncMex(String sender, String toEncrypt)
    {
	   String response;
	   try
	   {
		   // encrypt the message
		   EncryptedMessage enc = KeyManager.encMessage(toEncrypt, keyEncrypt);
		   // encode the whole message
		   response = enc.encoded();
	   }
	   catch(Exception ex)
	   {
		   response = ex.getMessage();
	   }
	   return response;
    }
	
	public class ConsoleLsaReceiver extends BroadcastReceiver {
		public static final String ACTION_RESP = "eu.sapere.intent.action.MESSAGE_PROCESSED";

		@Override
		public void onReceive(Context context, Intent intent) {
			String text[] = intent
					.getStringArrayExtra(SapereMiddlewareIntent.PARAM_OUT_MSG);
			String print = "";

			if (text != null)
				for (String s : text)
					print += s + "\n";

			TextView lsa = (TextView) findViewById(R.id.textLsa);
			lsa.setText(print + "\n");
		}
	}
	
	public class ConsoleGradientReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_GRADIENT_NUMBER";
        public static final String MEX_DEBUG="deb4";
        @Override
        public void onReceive(Context context, Intent intent) {
        	
           String text = intent.getStringExtra(ConsoleGradientReceiver.MEX_DEBUG);
           
           TextView console = (TextView) findViewById(R.id.textGradient);
           console.setText(text);
//   	   		console.setBackgroundColor(Color.CYAN);
        } 
	}
	
    
    public class ConsoleIdReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_ID_NUMBER";
        public static final String MEX_DEBUG="deb3";
        @Override
        public void onReceive(Context context, Intent intent) {
        	
           String text = intent.getStringExtra(ConsoleIdReceiver.MEX_DEBUG);
           
           TextView console = (TextView) findViewById(R.id.textId);
          
			try
			{
			   if (text.equals(""))
			   {
				   console.setText("");
				   return;
			   }
	           int h = Integer.parseInt(text)%(Global.ARRAY_ASCII_END - Global.ARRAY_ASCII_START);
	           String [] mex = Global.stringMessages();
	           console.setText(mex[h]);
	           console.setTextColor(Color.BLACK);
			}
			catch(Exception ex)
			{
				console.setText(ex.getMessage());
			}
        }        
    }
    
    public class ConsoleDebugReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_DEBUG";
        public static final String MEX_DEBUG="deb";
        @Override
        public void onReceive(Context context, Intent intent) {
           String text = intent.getStringExtra(ConsoleDebugReceiver.MEX_DEBUG);
           
           TextView console = (TextView) findViewById(R.id.textConsole);
           console.setText(text + "\n");
           
        }        
    }
    
    public class ConsoleLogReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_LOG";
        public static final String MEX_DEBUG="deb4";
        public static final String MEX_COLOR="mexColor";
        @Override
        public void onReceive(Context context, Intent intent) {
           String text = intent.getStringExtra(ConsoleLogReceiver.MEX_DEBUG);
           String color = intent.getStringExtra(ConsoleLogReceiver.MEX_COLOR);
           
           TextView console = (TextView) findViewById(R.id.textChatLog);
           console.append(text + "\n");
           
            if (color.equals(Global.COLOR_CHANNEL_1))
	        {
	        		console.setBackgroundColor(Color.CYAN);
	        }
	        else if (color.equals(Global.COLOR_CHANNEL_2))
	        {
		    		console.setBackgroundColor(Color.YELLOW);
	        }
	        else
	        {
	    			console.setBackgroundColor(Color.RED);
	        }

            console.setTextColor(Color.BLACK);
        }        
    }
    
    public class ConsoleDebugMessagesReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_DEBUG_SCREEN___";
        public static final String MEX_DEBUG="deb5";
        public static final String MEX_COLOR="color";
        @Override
        public void onReceive(Context context, Intent intent) {
           String text = intent.getStringExtra(ConsoleDebugMessagesReceiver.MEX_DEBUG);
           String color = intent.getStringExtra(ConsoleDebugMessagesReceiver.MEX_COLOR);
           
           TextView console = (TextView) findViewById(R.id.textDebugLog);
           console.append(text + "\n");
           
            if (color.equals(Global.COLOR_CHANNEL_1))
	        {
	        		console.setBackgroundColor(Color.CYAN);
	        }
	        else if (color.equals(Global.COLOR_CHANNEL_2))
	        {
		    		console.setBackgroundColor(Color.YELLOW);
	        }
	        else
	        {
	    			console.setBackgroundColor(Color.RED);
	        }

            console.setTextColor(Color.BLACK);
        }        
    }
    
    public class ConsoleColorReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_COLOR";
        public static final String MEX_DEBUG="deb2";
        @Override
        public void onReceive(Context context, Intent intent) {     	
	        	// get the name of the file
	        String file = intent.getStringExtra(ConsoleColorReceiver.MEX_DEBUG);
	        TextView v;
	        if (file.equals(Global.COLOR_CHANNEL_1))
	        {
	        		v = (TextView)findViewById(R.id.textColorC1);
	        		v.setBackgroundColor(Color.CYAN);
	        		v.setTextColor(Color.BLACK);
	        		v.setText(Global.NODE_SENDER1);
	        }
	        else
	        {
		    		v = (TextView)findViewById(R.id.textColorC2);
		    		v.setBackgroundColor(Color.YELLOW);
	        		v.setTextColor(Color.BLACK);
		    		v.setText(Global.NODE_SENDER2);
	        }
        }
    }
    
    public class ConsoleDisableButton extends BroadcastReceiver {
        public static final String ACTION_RESP = "sapere.intent.action.MESSAGE_DISABLE";
        public static final String MEX_DEBUG="dis_button";
        @Override
        public void onReceive(Context context, Intent intent) {
           Button console = (Button) findViewById(R.id.buttonSend);
           console.setVisibility(View.GONE);   
        }        
    }
    
    
	private void stopServices() {

		// for all intents
		Log.d("Quit", "Intento stopped!");
	}

	protected void onPause() {
		super.onPause();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d("Quit", "onKeyDown called");
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			stopServices();
			// bt.stopBtManager();
			stopService(msgIntent);

			onStop();
		}
		return false;

	}

	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(lsaReceiver);
		// unregisterReceiver(BtManager.ResponseReceiver);
		System.exit(0);

	}

	protected void onStop() {
		super.onStop();
		finish();

	}

}
