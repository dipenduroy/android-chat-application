/*
 * A class implementing our security functionalities.
 * 
 * GPL v.3
 * 
 * Francesco De Angelis
 * 
 * francesco.deangelis@unige.ch
 * 
 * 
 */

package securitymanager;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

public class KeyManager 
{
	private KeyPair keyPair;
	private final int PB_KEY_SIZE = 2048;
	public final static String PB_ALG = "RSA";
	public final static String SY_ALG = "AES";
	public final static String PB_ALG_SP = PB_ALG + "/ECB/PKCS1Padding";
	public final static String SY_SP = SY_ALG + "/CBC/PKCS5Padding";
	
	public KeyManager() throws Exception
	{
		// generate a public key
		keyPair = generatePublicKey();
	}

	public void regeneratePKey() throws Exception
	{
		// generate a new public key
		keyPair = generatePublicKey();	
	}
	
	public PublicKey getGeneratedPublicKey()
	{
		return keyPair.getPublic();
	}
	
	private KeyPair generatePublicKey() throws Exception
	{
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(PB_ALG);
		keyPairGenerator.initialize(PB_KEY_SIZE);
		
	    return keyPairGenerator.genKeyPair();
	}
	
	public EncryptedMessage encMessage(String mex) throws Exception
	{	
		// encrypt the message by using a symmetric cipher
		EncryptedMessage encMex = encryptSymmetric(mex.getBytes());
		// encrypt the session key with the public key
		encryptSessionKey(encMex, keyPair.getPublic());
		return encMex;
	}
	
	public static EncryptedMessage encMessage(String mex, PublicKey key) throws Exception
	{
		// encrypt the message by using a symmetric cipher
		EncryptedMessage encMex = encryptSymmetric(mex.getBytes());
		// encrypt the session key with the given public key
		encryptSessionKey(encMex, key);
		return encMex;
	}
	
	private static EncryptedMessage encryptSymmetric(byte[] input) throws Exception
	{
		EncryptedMessage enc = new EncryptedMessage();
		// create a session key
		SecureRandom sr = new SecureRandom();
		// AES 128
		enc.sessionKey = new byte[16];
		enc.iv = new byte[16];
		sr.nextBytes(enc.sessionKey);
		sr.nextBytes(enc.iv);
		// create an AES cipher
		Cipher cipher = Cipher.getInstance(SY_SP);
		cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(enc.sessionKey,SY_ALG), 
				new IvParameterSpec(enc.iv));
		enc.body = cipher.doFinal(input);
		return enc;
	}
	
	private static void encryptSessionKey(EncryptedMessage encMex, PublicKey key) throws Exception
	{
		byte [] result;
		Cipher cipher = Cipher.getInstance(PB_ALG_SP);
		
		// Initialize PBE Cipher with key and parameters
		cipher.init(Cipher.ENCRYPT_MODE, key);
		// Encrypt the encoded Private Key with the PBE key
		encMex.sessionKey = cipher.doFinal(encMex.sessionKey);
		encMex.iv = cipher.doFinal(encMex.iv);
	}
	
	public EncryptedMessage decMessage(EncryptedMessage encMex) throws Exception
	{	
		// decrypt the session key and the initializator vector
		decryptSessionKey(encMex, keyPair);
		// decrypt the message
		decryptSymmetric(encMex);
		return encMex;
	}
	
	private void decryptSessionKey(EncryptedMessage encMex, KeyPair rsaKey) throws Exception
	{
		byte [] result;
		Cipher cipher = Cipher.getInstance(PB_ALG_SP);
		// Initialize cipher
		cipher.init(Cipher.DECRYPT_MODE, rsaKey.getPrivate());
		// Decrypt the encoded Private Key with the PBE key
		encMex.sessionKey = cipher.doFinal(encMex.sessionKey);
		encMex.iv = cipher.doFinal(encMex.iv);
	}
	
	private void decryptSymmetric(EncryptedMessage encMex) throws Exception
	{
		Cipher cipher = Cipher.getInstance(SY_SP);
		cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(encMex.sessionKey,SY_ALG), 
				new IvParameterSpec(encMex.iv));
		encMex.body = cipher.doFinal(encMex.body);
	}
	
	public String getEncodedPKey()
	{
		byte[] encodedPublicKey =  keyPair.getPublic().getEncoded();
		StringBuffer encodedKey = new StringBuffer();
		encodedKey.append(encodedPublicKey[0]);	
		for(int i = 1; i < encodedPublicKey.length; i++ )
			encodedKey.append(";" + encodedPublicKey[i]);
		
		return encodedKey.toString();
	}
	
	public static PublicKey getDecodedPKey(String str)
	{

		String[] myKeyBack = str.split(";");
		byte[] encodedPublicKeyBack = new byte[myKeyBack.length];
		
		for(int j = 0; j < myKeyBack.length; j++){
			byte temp = new Byte(myKeyBack[j]);
			encodedPublicKeyBack[j] = temp;
		}
		 // decode the public key
		  KeySpec kSpec = new X509EncodedKeySpec(encodedPublicKeyBack);
		  
		  PublicKey decPk;
		  
		  try
		  {
		   // re-generate the public key
		   decPk = KeyFactory.getInstance(KeyManager.PB_ALG).generatePublic(kSpec);
		  }
		  catch(Exception ex)
		  {
			  return null;
		  }
		  return decPk;
	}
}
